import org.junit.Test;

import static org.junit.Assert.*;

public class MyParserTest {

    @Test
    public void init() {
        MyParser parser = new MyParser();
        try {
            parser.init("(ATOM 1)");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(parser.scanner != null, true);

    }

    @Test
    public void parseExpression() {
        MyParser parser = new MyParser();
        try {
            parser.init("(ATOM 1)");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            assertEquals(parser.parseExpression(true) instanceof Node, true);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}