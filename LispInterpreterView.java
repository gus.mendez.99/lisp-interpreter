import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.file.Paths;

/**
 * This class's purpose is to show the UI and execute the expressions and apply the functions in the
 * program, to later show the result in a textBox implementing Swing
 */
public class LispInterpreterView {


    /**
     * TextArea for show the results
     */
    TextArea input;
    TextArea result;

    String outputToProcess = "";

    /**
     * File chooser for pick the text file with the math expressions
     */
    FileChooser fileChooser ;

    /**
     * For parse
     **/
    private static MyParser myParser;

    /**
     * A hashSet to store the numericFunctionSet
     */
    private static HashSet<String> numericFunctionSet = new HashSet<String>(Arrays.asList("PLUS", "MINUS", "TIMES", "LESS", "GREATER", "QUOTIENT", "REMAINDER"));
    /**
     * A HashSet used to store the keywords from lisp
     */
    private static HashSet<String> keywords = new HashSet<String>(Arrays.asList("T", "NIL", "CAR", "CDR", "CONS", "ATOM", "EQ", "NULL",
            "INT", "PLUS", "MINUS", "TIMES", "LESS", "GREATER", "QUOTIENT", "REMAINDER", "COND", "QUOTE", "DEFUN", "LIST"));
    private static LinkedList<HashMap<String, Node>> funArgumentsList;
    private static LinkedList<Node> funDefinitionList = new LinkedList<Node>();


    /**
     * To show the JavFX UI
     *
     * @param stage Stage of JavaFX where we're gonna render the UI
     */
    public void show(Stage stage) {
        input = new TextArea("");
        result = new TextArea("...");
        fileChooser = new FileChooser();

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.lisp")
        );

        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        fileChooser.setInitialDirectory(new File(currentPath));

        BorderPane border = new BorderPane();
        HBox hbox = addHBox(stage);
        border.setTop(hbox);
        border.setLeft(addVBox());

        Scene scene = new Scene(border, 800, 600);
        stage.setTitle("Interprete");
        //scene.getStylesheets().add(Calculadora.class.getResource("estilo.css").toExternalForm());
        stage.setScene(scene);
        stage.show();

    }

    /**
     * To return a HBox with two buttons for read file and clear TextArea
     *
     * @param stage Stage of JavaFX where we're gonna render the UI
     * @return a filled HBox to add to the UI
     */
    public HBox addHBox(Stage stage) {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(6);
        hbox.setStyle("-fx-background-color: #455a64;");

        //Button for load the text file
        Button buttonCurrent = new Button("Open Lisp file");
        buttonCurrent.setPrefSize(150, 20);
        buttonCurrent.setOnAction(e -> {
            File selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile != null) {
                ArrayList<String> myLines = readFile(selectedFile);
                for (String line : myLines) {

                    input.appendText("\n" + line);

                    //Replace all the symbol operands with our predefined reserved words
                    line = line.replace("+", "PLUS");
                    line = line.replace("-", "MINUS");
                    line = line.replace("*", "TIMES");
                    line = line.replace("/", "QUOTIENT");
                    line = line.replace("<", "LESS");
                    line = line.replace(">", "GREATER");
                    line = line.replace("%", "REMAINDER");
                    line = line.replace("=", "EQ");
                    String[] nonCommentLine = line.split(";");
                    line = nonCommentLine[0];

                    outputToProcess += "\n + line";

                    //input.appendText("\n" + line);

                }
            }
        });

        //This is the debug button
        Button buttonRun = new Button("Run");
        buttonRun.setPrefSize(100, 20);
        buttonRun.setStyle("-fx-background-color: #388e3c;");
        buttonRun.setOnAction(e -> {
            //Split the input by lines and store in an Array
            List<String> initLines = Arrays.asList(input.getText().split("\n"));
            String finalLine = "";

            for (String line : initLines) {

                if (line.contains("LIST") || line.contains("list")) {
                    //Replace quote as list
                    line = line.replace("list", "list(");
                    line = line.replace("LIST", "LIST(");

                    line = line.replaceFirst("\\)", "))");
                }

                //Replace all the symbol operands with our predefined reserved words
                line = line.replace("+", "PLUS");
                line = line.replace("-", "MINUS");
                line = line.replace("*", "TIMES");
                line = line.replace("/", "QUOTIENT");
                line = line.replace("<", "LESS");
                line = line.replace(">", "GREATER");
                line = line.replace("%", "REMAINDER");
                line = line.replace("=", "EQ");
                String[] nonCommentLine = line.split(";");
                line = nonCommentLine[0];


                finalLine += "\n" + line;
            }

            List<String> lines = Arrays.asList(finalLine.split("\n"));


            //initialize index of array in 0
            int index = 0;
            //variable to control errors
            boolean error = false;

            //while the parser dd == false, and there's no error and the index < the size of the line
            while (MyParser.dd == false && !error && index < lines.size()) {
                try {
                    //instance of a new parser
                    myParser = new MyParser();

                    String expressionPassed = "";
                    //sum 1 to the index if its an empty line or a null object reference
                    while (lines.get(index) == null || lines.get(index).isEmpty()) {
                        index++;
                    }
                    //assign to expression passed the index of the line plus "#" wich means end of expression indicator
                    expressionPassed = lines.get(index) + "#";

                    //while the expression doesn't have the same qty of end and start braces
                    while (!hasSameBraces(expressionPassed)) {
                        index++;
                        if (lines.get(index) != null) {
                            expressionPassed += lines.get(index) + "#";
                        }
                    }

                    System.out.println("Expression is: " + expressionPassed);
                    myParser.init(expressionPassed);
                    startInterpreting();
                    index++;
                } catch (Exception ex) {
                    // TODO Auto-generated catch block
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Se ha interrumpido la lectura por: ");
                    alert.setContentText(ex.getMessage());
                    alert.showAndWait();
                    error = true;
                }
            }
        });

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        result.appendText("Output" + "\n");

        //Button for clear the input data
        Button buttonProjected = new Button("Clear");
        buttonProjected.setPrefSize(100, 20);
        buttonProjected.setOnAction(e -> {
            input.clear();
            result.clear();
        });

        hbox.getChildren().addAll(buttonCurrent, buttonProjected, buttonRun);

        return hbox;
    }

    /**
     * Verifies if an expression has the same amount of opening braces as closing braces before passing the expression
     * to the execution part
     *
     * @param line a String representing the line with a expression
     * @return true if the amount of leftSide and rigth braces is equal, false otherwise
     */
    private boolean hasSameBraces(String line) {

        int countLeftBraces = line.length() - line.replaceAll("\\(", "").length();
        int countRightBraces = line.length() - line.replaceAll("\\)", "").length();

        return countLeftBraces == countRightBraces;

    }

    /**
     * For read a file line by line
     *
     * @param file File object which have the math operation
     * @return an ArrayList of String lines of the file
     */
    private ArrayList<String> readFile(File file) {
        ArrayList<String> lines = new ArrayList<>();
        BufferedReader bufferedReader = null;

        try {

            bufferedReader = new BufferedReader(new FileReader(file));
            String text;
            while ((text = bufferedReader.readLine()) != null) {
                lines.add(text);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(LispInterpreterView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LispInterpreterView.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                Logger.getLogger(LispInterpreterView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return lines;
    }


    /**
     * For add a TextArea to the screen, and show the result
     *
     * @return a filled HBox to add to the UI
     */
    public VBox addVBox() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);

        //Adding the TextArea to the VBox
        input.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        vbox.getChildren().add(input);

        result.setFont(Font.font("Arial", FontWeight.MEDIUM, 14));
        vbox.getChildren().add(result);


        return vbox;
    }


    /**
     * Interpreting the expression
     *
     * @throws Exception if it can´t be evaluated
     */
    public void startInterpreting() throws Exception {

        if (MyParser.dd == false) {
            //Construct parse tree for each top level expression
            Node root = MyParser.parseExpression(true);
            //Pretty-print the parse tree constructed above
            funArgumentsList = new LinkedList<HashMap<String, Node>>();
            root = evaluate(root, funArgumentsList, funDefinitionList);
            if (root != null) {
                System.out.print("> ");
                printExpression(root);
                System.out.println("\n");
                result.appendText("\n");
            }
        }

    }

    /**
     * Gets the expression as a String
     * @param root the root node of the tree
     * @return the String of the expression
     * @throws Exception if it is unparseable
     */
    private static String getExpression(Node root) throws Exception {
        String res = "";
        if (root != null) {
            if (root.isInner) {
                res += "(" + getExpression(root.leftSide) + "." + getExpression(root.rightSide) + ")";
            } else
                return root.value;
        }

        return res;
    }

    /**
     * Pretty printing the parse tree for top-level expression
     *
     * @param root the root Node, to start printing
     * @throws Exception if it can´t be printed
     */
    private void printExpression(Node root) throws Exception {

        if (root != null) {
            if (root.isInner) {
                System.out.print("(");
                result.appendText("(");
                printExpression(root.leftSide);
                //System.out.print(".");
                //result.appendText(".");
                printExpression(root.rightSide);
                System.out.print(")");
                result.appendText(")");
            } else
                System.out.print(root.value);
            result.appendText(root.value);
        }
    }

    /**
     * verify if a given Node is a list
     *
     * @param node the node to analize
     * @return true if node.value.equals "T" a
     */
    private static boolean isList(Node node) {

        if (!node.isInner && !node.value.equals("NIL"))
            return false;

        if (null_(node).value.equals("T") || isList(node.rightSide))
            return true;

        return false;
    }

    /**
     * Check the length of a node
     *
     * @param node the Node type oject to evaluate
     * @return an int 0 = no, 1 = yeah
     */
    private static int length(Node node) {

        if (node.value.equals("NIL") && !node.isInner)
            return 0;

        return 1 + length(node.rightSide);
    }

    /**
     * Calculat the number of nodes that are childs of an specific Node
     *
     * @param node the node to AnalYze
     * @return returns the num of Nodes
     */
    private static int numNodes(Node node) {
        if (!node.isInner) {
            return 1;
        }

        return 1 + numNodes(node.leftSide) + numNodes(node.rightSide);
    }

    /**
     * Checks if a given Node has a leftSide child.
     *
     * @param s the node to analyze
     * @return the leftSide Node in case the original node s has a Token
     * @throws Exception
     */
    private static Node car(Node s) throws Exception {
        if (numNodes(s) == 1) {
            throw new Exception("> error in car : " + s.value + " has only one node");
        }

        return s.leftSide;
    }

    /**
     * Evaluates the rightSide child of a Node
     *
     * @param s the node to evaluate
     * @return the Node that it is immediately to the rightSide
     * @throws Exception
     */
    private static Node cdr(Node s) throws Exception {
        if (numNodes(s) == 1) {
            throw new Exception("> error in cdr :  " + s.value + " has only one node");
        }

        return s.rightSide;
    }

    /**
     * Constructs a new pair of variables from any type (ATOM_NUMBER, ATOM_LITERAL)
     *
     * @param s1 first Node to add to the Pair
     * @param s2 second Node to add to the Pair
     * @return
     */
    private static Node cons(Node s1, Node s2) {
        Node s = new Node(true, "");
        s.leftSide = s1;
        s.rightSide = s2;
        return s;
    }

    /**
     * Verifies if an specific argument passed after atom is ATOM type or not
     *
     * @param s the node to verify if it is atom
     * @return the new value for atom (true = T)
     */
    private static Node atom(Node s) {
        if (numNodes(s) == 1)
            return new Node(false, "T");
        return new Node(false, "NIL");
    }

    /**
     * Verifies if a certain node is LiteralAtom type
     *
     * @param s the node to evaluate, with a value and a type
     * @return true if the value is between 'A' and 'Z', false otherwise
     */
    private static boolean isLiteralAtom(Node s) {
        if (numNodes(s) == 1) {
            if (s.value.charAt(0) >= 'A' && s.value.charAt(0) <= 'Z')
                return true;
        }
        return false;
    }

    /**
     * Evaluates if a given node is int type (creates a new Node of value T or NIL depending on wether
     * it is or not an int type Node)
     *
     * @param s the Node to evaluate
     * @return a new node, from not Inner type with value T or NIL
     */
    private static Node int_(Node s) {
        if (numNodes(s) == 1) {
            try {
                Integer.parseInt(s.value);
                return new Node(false, "T");
            } catch (Exception e) {
                return new Node(false, "NIL");
            }
        }
        return new Node(false, "NIL");
    }

    /**
     * Evaluates if a given node is string type (creates a new Node of value T or NIL depending on whether
     * it is or not a string type Node)
     *
     * @param s the node to evaluate
     * @return a Node with value T if the nodes does not have leftSide or rightSide nodes and contains \,NIL otherwise
     */
    private static Node string_(Node s) {
        if (numNodes(s) == 1 && s.value.contains("\"")) {
            try {
                return new Node(false, "T");
            } catch (Exception e) {
                return new Node(false, "NIL");
            }
        }
        return new Node(false, "NIL");
    }

    /**
     * Evaluates if a node's value is NIL type
     *
     * @param s Node to evaluate
     * @return a new with value T if param.value is equal to NIL and param does not have leftSide or rightSide nodes, NIL otherwise
     */
    private static Node null_(Node s) {
        if (numNodes(s) == 1) {
            if (s.value.equals("NIL")) {
                return new Node(false, "T");
            }
        }
        return new Node(false, "NIL");
    }

    /**
     * Verifies if the value of a node is equal to the value of another node
     *
     * @param s1 first node to compare
     * @param s2 second node to compare
     * @return instance of a Node with value T if the or NIL
     * @throws Exception if one or both nodes have more than one node (they´re not a single ATOM value)
     */
    private static Node eq(Node s1, Node s2) throws Exception {
        if (numNodes(s1) > 1 || numNodes(s2) > 1) {
            throw new Exception("> error in eq function: " + s1.value + " or/and " + s2.value + " have more than one node");
        }

        if (s1.value.equals(s2.value))
            return new Node(false, "T");

        return new Node(false, "NIL");
    }

    /**
     * Sums the value of two NumericAtom type nodes
     *
     * @param s1 the first node to sum
     * @param s2 the second node to sum
     * @return instance of a Node with the value of the sum
     * @throws Exception if one or both nodes have more than one node (they´re not a single ATOM value)
     */
    private static Node plus(Node s1, Node s2) throws Exception {
        if (numNodes(s1) > 1 || numNodes(s2) > 1) {
            throw new Exception("> error in  plus function: " + s1.value + " or/and " + s2.value + " have more than one node");
        }

        return new Node(false, String.valueOf(Integer.valueOf(s1.value) + Integer.valueOf(s2.value)));
    }

    /**
     * Operates a rest with the value of two NumericAtom type nodes
     *
     * @param s1 the minuend of the operation
     * @param s2 the substract of the operation
     * @return instance of a Node with the result of the rest
     * @throws Exception if one or both nodes have more than one node (they´re not a single ATOM value)
     */
    private static Node minus(Node s1, Node s2) throws Exception {
        if (numNodes(s1) > 1 || numNodes(s2) > 1) {
            throw new Exception("> error in minus function: " + s1.value + " or/and " + s2.value + " have more than one node");

        }

        return new Node(false, String.valueOf(Integer.valueOf(s1.value) - Integer.valueOf(s2.value)));
    }

    /**
     * Multiplies two NumericAtom type nodes
     *
     * @param s1 the first operand
     * @param s2 the second operand
     * @return instance of a Node with the result of the multiplication
     * @throws Exception if one or both nodes have more than one node (they´re not a single ATOM value)
     */
    private static Node times(Node s1, Node s2) throws Exception {
        if (numNodes(s1) > 1 || numNodes(s2) > 1) {
            throw new Exception("> error in times function: " + s1.value + " or/and " + s2.value + " have more than one node");
        }

        return new Node(false, String.valueOf(Integer.valueOf(s1.value) * Integer.valueOf(s2.value)));
    }

    /**
     * Operates the division between two NumericAtom type nodes
     *
     * @param s1 the dividend of the operation
     * @param s2 the divisor of the operation
     * @return instance of a Node with the quotient of the division as the value
     * @throws Exception if one or both nodes have more than one node (they´re not a single ATOM value)
     */
    private static Node quotient(Node s1, Node s2) throws Exception {
        if (numNodes(s1) > 1 || numNodes(s2) > 1) {
            throw new Exception("> error in quotient function: " + s1.value + " or/and " + s2.value + " have more than one node");
        }

        if (Integer.valueOf(s2.value) != 0)
            return new Node(false, String.valueOf((Integer.valueOf(s1.value)) / (Integer.valueOf(s2.value))));
        else
            throw new Exception("> error in  quotient function: denominator is zero");

    }

    /**
     * Operates a division between two NumericAtom type nodes and returns the reminder of the operation
     *
     * @param s1 the dividend of the operation
     * @param s2 the divisor of the operation
     * @return instance of a Node with the remainder of the division as the value
     * @throws Exception if one or both nodes have more than one node (they´re not a single ATOM value)
     */
    private static Node remainder(Node s1, Node s2) throws Exception {
        if (numNodes(s1) > 1 || numNodes(s2) > 1) {
            throw new Exception("> error in remainder function: " + s1.value + " or/and " + s2.value + " have more than one node");
        }

        if (Integer.valueOf(s2.value) != 0)
            return new Node(false, String.valueOf((Integer.valueOf(s1.value)) % (Integer.valueOf(s2.value))));
        else
            throw new Exception(" > error in remainder function: " + s2.value + " is zero");

    }

    /**
     * Verifies if the value of node 1 is less than value of node 2
     *
     * @param s1 allegedly minor node
     * @param s2 allegedly greater node
     * @return T if value of s1 is minor than value of s2, NIL otherwise
     * @throws Exception if one or both nodes have more than one node (they´re not a single ATOM value)
     */
    private static Node less(Node s1, Node s2) throws Exception {
        if (numNodes(s1) > 1 || numNodes(s2) > 1) {
            throw new Exception("> error in less function: " + s1.value + " or/and " + s2.value + " have more than one node");
        }

        if (Integer.valueOf(s1.value) < Integer.valueOf(s2.value))
            return new Node(false, "T");

        return new Node(false, "NIL");
    }

    /**
     * Verifies if the value of node 1 is greater than value of node 2
     *
     * @param s1 allegedly greater node
     * @param s2 allegedly minor node
     * @return T if value of s1 is greater than value of s2, NIL otherwise
     * @throws Exception if one or both nodes have more than one node (they´re not a single ATOM value)
     */
    private static Node greater(Node s1, Node s2) throws Exception {
        if (numNodes(s1) > 1 || numNodes(s2) > 1) {
            throw new Exception("> error in greater function: " + s1.value + " or/and " + s2.value + " have more than one node");
        }

        if (Integer.valueOf(s1.value) > Integer.valueOf(s2.value))
            return new Node(false, "T");

        return new Node(false, "NIL");
    }

    /**
     * Search in a Node HashMap contains a pair with a given key
     *
     * @param key   the key to search in the Node HashMap
     * @param aList the funArgumentsList that saves the pair of params from the function
     * @return true if pair is found, false otherwise
     */
    private static boolean bound(String key, LinkedList<HashMap<String, Node>> aList) {

        for (HashMap<String, Node> pair : aList) {
            if (pair.containsKey(key))
                return true;
        }
        return false;
    }

    /**
     * Gets a node from the funArgumentsList linkedList with an specific key provided
     *
     * @param key   the key of the Node to get
     * @param aList the funArgumentsList that saves the pair of params from the function
     * @return the Node if funArgumentsList contains it or null otherwise
     */
    private static Node getVal(String key, LinkedList<HashMap<String, Node>> aList) {

        for (HashMap<String, Node> pair : aList) {
            if (pair.containsKey(key))
                return pair.get(key);
        }
        return null;
    }

    /**
     * Obtains the Node that contains a function definition matching a given functionName
     *
     * @param functionName the name of the function as String
     * @param dList        the LinkedList that contains the Node with the function
     * @return The definition of the function
     * @throws Exception if the function doesn't exists in the funDefinitionList
     */
    private static Node getValFromDList(String functionName, LinkedList<Node> dList) throws Exception {

        try {
            for (Node funDef : dList) {
                if (car(funDef).value.equals(functionName))
                    return cdr(funDef);
            }
        } catch (Exception e) {
            throw new Exception("> error: undefined function - " + functionName);
        }

        throw new Exception(" > error: undefined function - " + functionName + ", not present in funDefinitionList");
    }

    /**
     * applies the changes and executes the function
     *
     * @param funName         the node that represents the function
     * @param actualParamList Node that contains the parameters of the function
     * @param aList           the list that contains the actual parameters of a function Definition
     * @param dList           the list that contains the functions as Nodes
     * @return the node of the evaluated function
     * @throws Exception
     */
    private static Node apply(Node funName, Node actualParamList, LinkedList<HashMap<String, Node>> aList, LinkedList<Node> dList) throws Exception {

        addPairs(car(getValFromDList(funName.value, dList)), actualParamList, aList);
        Node retValue = evaluate(cdr(getValFromDList(funName.value, dList)), aList, dList);
        removePairs(car(getValFromDList(funName.value, dList)), actualParamList, aList);
        return retValue;
    }

    /**
     * Adds the Node of the required param for a function and the Node of the passed params tho the funArgumentsList
     *
     * @param formalParamList the Node object that contains the required params for the function
     * @param actualParamList the Node object that contains the given params for the function
     * @param aList           the LinkedList that saves the pairs of the nodes specified above
     * @throws Exception
     */
    private static void addPairs(Node formalParamList, Node actualParamList, LinkedList<HashMap<String, Node>> aList) throws Exception {

        if (length(formalParamList) < length(actualParamList)) {
            throw new Exception("> too many arguments");
        } else if (length(formalParamList) > length(actualParamList)) {
            throw new Exception("> too few arguments");
        }
        while (null_(formalParamList).value.equals("NIL")) {

            HashMap<String, Node> pair = new HashMap<String, Node>();
            pair.put(formalParamList.leftSide.value, actualParamList.leftSide);
            aList.addFirst(pair);

            formalParamList = formalParamList.rightSide;
            actualParamList = actualParamList.rightSide;
        }
    }

    /**
     * Deletes the pairs from funArgumentsList once the function is executed
     *
     * @param formalParamList the Node object that contains the required params for the function
     * @param actualParamList the Node object that contains the given params for the function
     * @param aList           the LinkedList that saves the pairs of the nodes specified above
     * @throws Exception
     */
    private static void removePairs(Node formalParamList, Node actualParamList, LinkedList<HashMap<String, Node>> aList) throws Exception {

        if (length(formalParamList) != length(actualParamList)) {
            throw new Exception("> error in apply of function : number of actual parameter expressions do not match the number of formal paramters");

        }

        while (null_(formalParamList).value.equals("NIL")) {

            HashMap<String, Node> pair = new HashMap<String, Node>();
            pair.put(formalParamList.leftSide.value, actualParamList.leftSide);
            aList.removeFirstOccurrence(pair);

            formalParamList = formalParamList.rightSide;
            actualParamList = actualParamList.rightSide;
        }
    }

    /**
     * Passes the constructed expression to the evaluate() method
     *
     * @param actualParamList the Node object that contains the required params for the function
     * @param aList           the LinkedList that saves the pairs of the nodes specified above
     * @param dList           the LinkedList that contains the Node of the currentFunction
     * @return the actualParamList if value of it is null, otherwise it returns the constructed expression Node already evaluated
     * @throws Exception if it cannot construct and evaluate the expression
     */
    private static Node evList(Node actualParamList, LinkedList<HashMap<String, Node>> aList, LinkedList<Node> dList) throws Exception {
        try {
            if (null_(actualParamList).value.equals("T")) {
                return actualParamList;
            }

            return cons(evaluate(car(actualParamList), aList, dList), evList(cdr(actualParamList), aList, dList));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    /**
     * Evaluates a node (function), both UDF´s and proper Lisp functions are evaluated and changes are made callin apply() method
     *
     * @param s     Node to evaluate
     * @param aList the LinkedList that saves the pairs of the actual and given params to a function type node
     * @param dList LinkedList that contains the Functions Nodes
     * @return the resulting Node of the expression or function
     * @throws Exception if the expression is from undefined type, if it is not a list, if there are to many arguments to a given function,
     *                   if there are to few arguments to the function, if an arithmetic function is called with LITERAL_ATOM type or T or NIL.
     */
    private static Node evaluate(Node s, LinkedList<HashMap<String, Node>> aList, LinkedList<Node> dList) throws Exception {

        if (numNodes(s) == 1 && (s.value.equals("T") || s.value.equals("NIL") || int_(s).value.equals("T") || string_(s).value.equals("T")))

            //if (numNodes(s) == 1 && (s.value.equals("T") || s.value.equals("NIL") || int_(s).value.equals("T")))

            return s;

        if (numNodes(s) == 1 && bound(s.value, aList))
            return getVal(s.value, aList);

        if (numNodes(s) == 1) {
            throw new Exception("> error in evaluate function: " + getExpression(s) + " is atom of undefined type");
        }

        if (!isList(s)) {
            throw new Exception(" > error in evaluate function : " + getExpression(s) + " is not a list");

        }

        if (numericFunctionSet.contains(car(s).value)) {
            if (length(s) < 3) {
                throw new Exception("> error: too few arguments in " + (car(s).value) + " function");
            }

            if (length(s) > 3) {
                throw new Exception(" > error: too many arguments in " + (car(s).value) + " function");
            }

            Node s1 = evaluate(car(cdr(s)), aList, dList);
            Node s2 = evaluate(car(cdr(cdr(s))), aList, dList);

            if (!int_(s1).value.equals("T") || !int_(s2).value.equals("T") || isLiteralAtom(s1) || isLiteralAtom(s2) || string_(s1).value.equals("T")
                    || string_(s2).value.equals("T")) {

                //if (!int_(s1).value.equals("T") || !int_(s2).value.equals("T") || isLiteralAtom(s1) || isLiteralAtom(s2)) {
                throw new Exception("> error in " + car(s).value + " function: function can be performed only on numeric atoms");
            }

            switch (car(s).value) {

                case "PLUS":
                    return plus(s1, s2);

                case "MINUS":
                    return minus(s1, s2);

                case "TIMES":
                    return times(s1, s2);

                case "LESS":
                    return less(s1, s2);

                case "GREATER":
                    return greater(s1, s2);

                case "QUOTIENT":
                    return quotient(s1, s2);

                case "REMAINDER":
                    return remainder(s1, s2);
            }
        }

        if (car(s).value.equals("EQ")) {
            if (length(s) != 3) {
                throw new Exception(" > error in " + car(s).value + " function: length of the lisp-expression is not equal to 3");

            }

            Node s1 = evaluate(car(cdr(s)), aList, dList);
            Node s2 = evaluate(car(cdr(cdr(s))), aList, dList);

            if (!atom(s1).value.equals("T") || !atom(s2).value.equals("T")) {
                throw new Exception("> error in " + car(s).value + " function: function can be performed only on numeric atoms");

            }

            return eq(s1, s2);
        }

        if (car(s).value.equals("LIST")) {
            //TODO: Still have to think about the logic of List object...
            /*Node s1 = evaluate(car(cdr(s)), funArgumentsList, funDefinitionList);
            Node s2 = evaluate(car(cdr(cdr(s))), funArgumentsList, funDefinitionList);

            if(!atom(s1).value.equals("T") || !atom(s2).value.equals("T")) {
                throw new Exception("> error in " + car(s).value + " function: function can be performed only on numeric atoms");

            }

            return eq(s1, s2);*/
        }

        if (car(s).value.equals("ATOM") || car(s).value.equals("INT") || car(s).value.equals("NULL")) {
            if (length(s) != 2) {
                throw new Exception("> error in " + car(s).value + " function: length of the lisp-expression is not equal to 2");

            }

            Node s1 = evaluate(car(cdr(s)), aList, dList);

            switch (car(s).value) {

                case "ATOM":
                    return atom(s1);

                case "INT":
                    return int_(s1);

                case "NULL":
                    return null_(s1);
            }
        }

        if (car(s).value.equals("CAR") || car(s).value.equals("CDR")) {
            if (length(s) != 2) {
                throw new Exception(" > error in " + car(s).value + " function: length of the lisp expression is not equal to 2");

            }

            Node s1 = evaluate(car(cdr(s)), aList, dList);

            switch (car(s).value) {

                case "CAR":
                    return car(s1);

                case "CDR":
                    return cdr(s1);
            }
        }

        if (car(s).value.equals("CONS")) {
            if (length(s) != 3) {
                throw new Exception("> error in " + car(s).value + " function: length of the lisp-expression is not equal to 3");

            }

            Node s1 = evaluate(car(cdr(s)), aList, dList);
            Node s2 = evaluate(car(cdr(cdr(s))), aList, dList);

            return cons(s1, s2);
        }

        if (car(s).value.equals("QUOTE")) {
            if (length(s) != 2) {
                throw new Exception("> error in " + car(s).value + " function: length of the lisp-expression is not equal to 2");

            }
            return car(cdr(s));
        }

        if (car(s).value.equals("LIST")) {
            if (length(s) != 2) {
                throw new Exception("> error in " + car(s).value + " function: length of the lisp-expression is not equal to 2");

            }
            return car(cdr(s));
        }

        if (car(s).value.equals("COND")) {

            Node remaining = cdr(s);
            if (remaining.value.equals("NIL")) {
                throw new Exception("> error in " + car(s).value + " too few arguments");
            }

            Node s1 = car(remaining);

            while (true) {
                if (!isList(s1)) {
                    throw new Exception("> error in  " + car(s).value + " : one of lisp-expressons is not a list");

                }

                if (length(s1) != 2) {
                    throw new Exception(" > error in " + car(s).value + " : length of one of the lisp-expressions is not equal to 2");

                }

                Node b = car(s1);
                b = evaluate(b, aList, dList);

                if (!b.value.equals("NIL")) {
                    Node e = car(cdr(s1));
                    return evaluate(e, aList, dList);
                }

                if (null_(cdr(remaining)).value.equals("NIL")) {
                    s1 = car(cdr(remaining));
                    remaining = cdr(remaining);
                } else {
                    throw new Exception(" > error in " + car(s).value + " : all conditions evaluating to NIL");

                }
            }
        }

        if (car(s).value.equals("DEFUN")) {
            if (length(s) != 4) {
                throw new Exception(" > error in " + car(s).value + " : length of list is not equal to 4");
            }

            Node funName = car(cdr(s));

            if (!isLiteralAtom(funName)) {
                throw new Exception(" > error in " + car(s).value + " : function name is not a literal atom");

            }

            if (keywords.contains(funName.value)) {
                throw new Exception("> error in " + car(s).value + " : function name cannot be " + funName.value);

            }

            Node paramList = car(cdr(cdr(s)));
            Node paramListCurr = paramList;
            Node body = car(cdr(cdr(cdr(s))));

            if (!isList(paramList)) {
                throw new Exception(" > error in " + car(s).value + " : formal parameter list is not a list");

            }

            HashSet<String> paramNameSet = new HashSet<String>();

            while (null_(paramListCurr).value.equals("NIL")) {

                if (!isLiteralAtom(car(paramListCurr))) {
                    throw new Exception("> error in " + car(s).value + " : formal paramter is not a literal atom");

                }

                if (keywords.contains(car(paramListCurr).value)) {
                    throw new Exception("> error in " + car(s).value + " : formal paramter name cannot be " + car(paramListCurr).value);

                }

                if (paramNameSet.contains(car(paramListCurr).value)) {
                    throw new Exception("> error in " + car(s).value + " : duplicate formal paramter name found");

                }

                paramNameSet.add(car(paramListCurr).value);
                paramListCurr = paramListCurr.rightSide;
            }


            Node funDefn = new Node(true, "");
            funDefn.leftSide = funName;
            funDefn.rightSide = new Node(true, "");
            funDefn.rightSide.leftSide = paramList;
            funDefn.rightSide.rightSide = body;
            dList.add(funDefn);

            return new Node(false, funName.value);

        }

        return apply(car(s), evList(cdr(s), aList, dList), aList, dList);
    }
}
