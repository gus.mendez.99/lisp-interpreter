/**
 * This class represents each reserved word in the lisp enviroment
 * including logical operators, arithmetic operators UDF's names, and
 * predicates
 * for example: DEFUN, GREATER, MINUS, PLUS
 *
 * It can also represent a character that can be a syntax char or a value
 * like a NumericAtom, LiteralAtom, OpenParenthesis, ClosingParenthesis, DOT, EOF (End Of File),
 * EOS (End Of String).
 * for example: ( ) . 1 2 $ $$
 * @author Luis Urbina
 * @version 1.0
 * @since 3/12/2019
 */
public class Token {
    //The value that the token can take
    String value;
    //Any of the types that the token could be (explained in javadoc above)
    String type;

    /**
     * Constructs a new Token with given value and type (both string objects)
     * @param value the char of the token
     * @param type the type that the token could be (explained in javadoc above)
     */
    Token(String value, String type) {
        this.value = value;
        this.type = type;
    }
}