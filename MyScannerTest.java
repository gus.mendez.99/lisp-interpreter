import org.junit.Test;

import static org.junit.Assert.*;

public class MyScannerTest {

    @Test
    public void init() {

        MyScanner scanner = new MyScanner();

        try {
            scanner.init("(ATOM 1)");
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(scanner.getCurrentToken() != null, true);


    }

    @Test
    public void MoveToNext(){
        MyScanner scanner = new MyScanner();

        try {
            scanner.init("(ATOM 1)");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            assertEquals(scanner.MoveToNext(), false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}