/**
 * This class represents every single expression that the Lisp Interpreter can
 * manage, it has the isInner attribute to represent if it is Inner relative
 * to another Node (a leaf) or if it is not (true) in the tree-like structure that will be
 * constructed to execute expressions.
 *
 * Every node has a reference to the leftSide and to the rightSide Node immediately
 * next to it in the structure.
 * @author Luis Urbina
 * @version 1.0
 * @since 3/12/2019
 */
public class Node {
    boolean isInner;
    //value of the node, represented as a string
    String value;
    //reference to the node immediately to the leftSide
    Node leftSide;
    //reference to the node immediately to the rightSide
    Node rightSide;

    /**
     * Constructs a new node indicating ig it isInner node and with an specific String-type value
     * @param isInner true if it is Inner to another Node, false if not
     * @param value the value of the single Node as a String
     */
    Node(boolean isInner, String value) {
        this.isInner = isInner;
        this.value = value;

        if(!value.equals("NIL")) {
            this.leftSide = new Node(false,"NIL");
            this.rightSide = new Node(false,"NIL");
        }
    }
}