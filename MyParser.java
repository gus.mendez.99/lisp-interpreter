/**
 * MyParser Class is used to "parse" and convert them to a low level instruccion
 * the expressions once they are passed from myScanner
 * principal method returns a node with all the validations to pass it to LispInterpreterView
 * @author Gustavo Méndez
 * @version 1.2
 * @since 3/14/2019
 */
public class MyParser {
    static MyScanner scanner;
    static boolean dd;

    /**
     * create an instance of the scanner and initialize it
     * @param line to pass the line of an expression
     * @throws Exception If the current expression can´t be parsed
     */
    public void init(String line) throws Exception {
        scanner = new MyScanner();
        scanner.init(line);
    }

    /**
     * Returns a Node but in a ready-to-evaluate form
     * @param isFirstTimeTyping true if it is the first time the char is passed
     * @return the Node of the parsed Expression
     * @throws Exception
     */
    public static Node parseExpression(boolean isFirstTimeTyping) throws Exception {
        //assign a null value to the root of the tree-like structure
        Node root = null;

        //Create a leaf node corresponding to the atom and return it
        if(scanner.getCurrentToken().type==MyScanner.LITERAL_ATOM ||
                scanner.getCurrentToken().type==MyScanner.NUMERIC_ATOM || scanner.getCurrentToken().type == MyScanner.STRING_ATOM) {
            root = new Node(false,scanner.getCurrentToken().value);
            dd = scanner.MoveToNext();
        }

        //Parse the list of expressions and construct the subtree corresponding to that
        //if the currentNode equals an OPEN_BRACE_PARENTHESIS
        else if(scanner.getCurrentToken().type==MyScanner.OPEN_BRACE_PARENTHESIS) {
            //move the scanner
            scanner.MoveToNext();
            //current node = root Node
            Node currentNode = root;

            //while there´s not a closing parenthesis
            while(scanner.getCurrentToken().type!=MyScanner.CLOSING_BRACE_PARENTHESIS) {
                //if a token with EOF type is found, exit program
                if(scanner.getCurrentToken().type==MyScanner.EOF){
                    System.exit(0);
                }
                //if the current token = $ or the end of an input
                else if(scanner.getCurrentToken().type == MyScanner.EOS){
                    //move to the next Line
                    dd=scanner.MoveToNext();
                    //throew exception, there was not a closing parenthesis
                    throw new Exception("> -- ERROR: unexpected dollar-- ");
                }else{
                    //if currentToken is DOT type
                    if(scanner.getCurrentToken().type == MyScanner.DOT){
                        //pass to next Token
                        dd=scanner.MoveToNext();
                        //instance of new Node form recursive call
                        Node subtree = parseExpression(false);
                        if(scanner.getCurrentToken().type == MyScanner.DOT){
                            throw new Exception("> -- ERROR: unexpected dot-- ");
                        }else
                            currentNode.rightSide = subtree;
                    }else{
                        //if theres a current one
                        if(currentNode!=null){
                            //add a new node as a subtree, to the leftSide and set that new Node as current
                            currentNode.rightSide = new Node(true, "");
                            currentNode = currentNode.rightSide;
                            //node instance with recursive Call of the function
                            Node subtree = parseExpression(false);
                            //add subtree´to the leftSide of the now current Node
                            currentNode.leftSide = subtree;
                        }
                        else {
                            //instance a new node with value, and then set the current
                            root = new Node(true, "");
                            currentNode = root;
                            currentNode.leftSide = parseExpression(false);
                        }
                    }
                }

            }

            dd=scanner.MoveToNext();
        }

        //If token is neither an atom nor an Open parenthesis, it is an error
        else if(scanner.getCurrentToken().type==MyScanner.EOF){
            System.exit(0);
        }

        else if(scanner.getCurrentToken().type == MyScanner.EOS){
            dd=scanner.MoveToNext();
            return parseExpression(true);
        }

        else if(scanner.getCurrentToken().type == MyScanner.CLOSING_BRACE_PARENTHESIS)
            throw new Exception("> -- ERROR: unexpected closing paranthesis without opening paranthesis --");
        else if(scanner.getCurrentToken().type == MyScanner.DOT)
            throw new Exception("> -- ERROR: unexpected dot-- ");

        if(isFirstTimeTyping && scanner.getCurrentToken().type!=MyScanner.EOF && scanner.getCurrentToken().type!=MyScanner.EOS) {
            throw new Exception("> -- ERROR: $ or $$ expected --");
        }
        //returning the node
        if(root!=null)
            return root;
        else
            return new Node(false,"NIL");
    }
}