import java.util.Scanner;

/**
 * This class gets a string value in the init method, and iterates over the string to get
 * the value of each character and generate Token type objects with a specific value according to the type of
 * characters that it is identified, predefined types of Tokens are shown below as static type Strings.
 * Also, a scanner type object is used to change the currentToken line of an input to scan it individually.
 * @Author Luis Urbina
 * @Version 1.1
 * @Since 3/12/2019
 */

public class MyScanner {
    //Scanner type object to analize each line
    private static Scanner scanner;
    //a single input Line
    static String passedExpressionLine = null;
    //position to iterate in the passedExpressionLine
    private static int position = 0;
    //currentToken instance of a single Token
    private Token currentToken;

    //constants for all the posible type of tokens
    public static final String EOF = "EOF";
    public static final String DOT = "DOT";
    public static final String EOS = "EOS";
    //Types set as ATOM
    public static final String NUMERIC_ATOM = "NumericAtom";
    public static final String LITERAL_ATOM = "LiteralAtom";
    public static final String STRING_ATOM = "StringAtom";
    // Fro parenthesis braces
    public static final String OPEN_BRACE_PARENTHESIS = "OpenParenthesis";
    public static final String CLOSING_BRACE_PARENTHESIS = "ClosingParenthesis";


    /**
     * Creates an instance of Scanner, sets # symbol as a delimiter to jump to the next expression
     * and assigns the value of the first line of the whole input to the passedExpressionLine attribute
     * @param line
     * @throws Exception
     */
    public void init(String line) throws Exception {
        scanner = new Scanner(line + "$");
        position = 0;
        scanner.useDelimiter("#");
        passedExpressionLine = scanner.nextLine();

        //if passedExpressionLine is empty and the scanner has next Line
        while(passedExpressionLine.isEmpty() && scanner.hasNextLine()) {
            //assign scanner.nextLine to the currentToken InputLine
            passedExpressionLine = scanner.nextLine();
        }
        //get the nextToken
        currentToken = getNextToken(scanner);
    }

    /**
     * get currentToken token
     * @return the currentToken Token-type object
     */
    public Token getCurrentToken() {
        return currentToken;
    }

    /**
     * Reassigns the value of currentToken and  returns true if the currentToken Token value is the $$ char,
     * this means, EOF
     * @return true if currentToken Token type = $$ (end of file), false otherwise
     * @throws Exception
     */
    public boolean MoveToNext() throws Exception {
        currentToken = getNextToken(scanner);
        return (currentToken.type==EOF);
    }

    /**
     * This method analyzes the different cases for each char of a string read form scanner (passedExpressionLine)
     * then it creates and returns a Token with the value of the char and the type of it
     * @param scanner the scanner object to make actions of change lines and read lines
     * @return the instance of a Token
     * @throws Exception in case the char is invalid, not an Atomic-Number or a reserved word
     */
    private static Token getNextToken(Scanner scanner) throws Exception{
        //if the token starts with an int
        boolean startsWithNumber = false;
        //if the token starts with another char
        boolean startsWithChar = false;
        //if it starts with none of the above
        boolean isError = false;
        int i;

        //if currentToken line is null (for first call of getNextToken) or currentToken line has been parsed completely
        if(passedExpressionLine == null || passedExpressionLine.substring(position).isEmpty()) {
            //position will be at 0
            position = 0;
            //passedExpressionLine will be reassigned
            passedExpressionLine = scanner.nextLine();

            //if passedExpressionLine is empty, load next line to inputLIne
            while(passedExpressionLine.isEmpty() && scanner.hasNextLine()) {
                passedExpressionLine = scanner.nextLine();
            }

        }

        //if passedExpressionLine is $$ create a token of End of File Type
        if(passedExpressionLine.equals("$$"))
            return new Token("$$",EOF);
        //if passedExpressionLine is $ create a token of End of String type
        if(passedExpressionLine.equals("$")){
            passedExpressionLine =null;
            return new Token("$",EOS);
        }

        int startPos = position;
        //iterate over the passedExpressionLine
        for(i=startPos; i< passedExpressionLine.length(); i++) {

            //if the char is a whiteSpace, a line change or a #
            if(passedExpressionLine.charAt(i)==' ' || passedExpressionLine.charAt(i)=='\n' || passedExpressionLine.charAt(i)=='#') {
                //settle the currentToken index as the firstSpaceIndex
                int firstSpaceIndex = i;

                //capture all consecutive ' ', '\n'
                while(i< passedExpressionLine.length() && (passedExpressionLine.charAt(i)==' ' || passedExpressionLine.charAt(i)=='\n' || passedExpressionLine.charAt(i)=='#'))
                    i++;

                // if currentToken line has ended and we have found no token
                if(!startsWithChar && !startsWithNumber && i== passedExpressionLine.length()) {

                    if(!scanner.hasNext()) {
                        position = i;
                    }

                    //reset passedExpressionLine to next line in input and reset position/startPos/i to 0
                    else {
                        startPos = 0;
                        position = 0;
                        passedExpressionLine = scanner.nextLine();

                        while(passedExpressionLine.isEmpty() && scanner.hasNextLine()) {
                            passedExpressionLine = scanner.nextLine();
                        }

                        i = -1;
                        continue;
                    }
                }
                //if the inputStarts with a char
                else if(startsWithChar) {
                    //save the currentToken Position
                    position =i;
                    //return instance of a token with with the value between the startPoisiton and the firstSpaceIndex
                    //and it is LITERAL_ATOM type
                    return new Token(passedExpressionLine.substring(startPos, firstSpaceIndex),LITERAL_ATOM);
                }
                //if startsWithNumber = true
                else if(startsWithNumber) {
                    //save the currentToken position
                    position =i;
                    //verify if it is a valid NUMERIC_ATOM

                    if(!isError)
                        //return instance of a token with with the value between the startPoisiton and the firstSpaceIndex
                        //and it is LITERAL_ATOM type
                        return new Token(passedExpressionLine.substring(startPos, firstSpaceIndex),NUMERIC_ATOM);

                    else
                        throw new Exception("> **error: Invalid token wrong identifier**");

                }
            }
            if(passedExpressionLine.charAt(i) == '(') {

                //if '(' is the first character to be parsed
                if(i== position) {
                    position++;
                    //step rightSide one position and return a new Token (OPEN_BRACE_PARENTHESIS type)
                    return new Token("(", OPEN_BRACE_PARENTHESIS);
                }

                else {
                    //return a NUMERIC_ATOM OR LITERAL_ATOM Token depending on what is found
                    position = i;
                    if(startsWithNumber)  {
                        if(!isError)
                            return new Token(passedExpressionLine.substring(startPos, i),NUMERIC_ATOM);
                        else
                            throw new Exception("> **error: Invalid token wrong identifier**");

                    }

                    if(startsWithChar)
                        return new Token(passedExpressionLine.substring(startPos, i),LITERAL_ATOM);

                    position = i+1;
                    return new Token("(", OPEN_BRACE_PARENTHESIS);
                }
            }

            else if(passedExpressionLine.charAt(i) == '.'){
                //if a dot is found, step rightSide one position and return de DOT type Token
                if(i== position){
                    position++;
                    return new Token(".", DOT);
                }
                else{
                    //sets the new position and returns a NUMERIC_ATOM Token
                    position = i;
                    if(startsWithNumber) {
                        if(!isError)
                            return new Token(passedExpressionLine.substring(startPos, i),NUMERIC_ATOM);
                        else
                            throw new Exception("**error: Invalid token wrong identifier**");

                    }
                    //otherwise returns a Literal_ATOM
                    if(startsWithChar)
                        return new Token(passedExpressionLine.substring(startPos, i),LITERAL_ATOM);

                    position = i+1;
                    return new Token(".", DOT);
                }
            }

            //If an ender parenthesis is found
            else if(passedExpressionLine.charAt(i) == ')') {
                if(i== position) {
                    //update position and return a new CLOSING_BRACE_PARENTHESIS Token
                    position++;
                    return new Token(")", CLOSING_BRACE_PARENTHESIS);
                }
                else {
                    //set new position
                    position = i;
                    //return a new instance of Token, depending on wether it is a NUMERIC_ATOM OR A LITERAL_ATOM
                    if(startsWithNumber) {
                        if(!isError)
                            return new Token(passedExpressionLine.substring(startPos, i),NUMERIC_ATOM);
                        else
                            throw new Exception("> **error: Invalid token wrong identifier**");
                    }

                    if(startsWithChar)
                        return new Token(passedExpressionLine.substring(startPos, i),LITERAL_ATOM);

                    position = i+1;
                    return new Token(")", CLOSING_BRACE_PARENTHESIS);
                }
            }

            else if(passedExpressionLine.charAt(i)== '$'){
                if(i== position) {
                    position++;
                    return new Token("$", EOS);
                }
                else {
                    position = i;
                    if(startsWithNumber) {
                        if(!isError)
                            return new Token(passedExpressionLine.substring(startPos, i),NUMERIC_ATOM);
                        else
                            throw new Exception("> **error: Invalid token wrong identifier**");

                    }

                    if(startsWithChar)
                        return new Token(passedExpressionLine.substring(startPos, i),LITERAL_ATOM);

                    position = i+1;
                    return new Token("$", EOS);
                }
            }

            //TODO: Read Strings with double quotes
            else if((passedExpressionLine.charAt(i)>='A' && passedExpressionLine.charAt(i)<='Z') || (passedExpressionLine.charAt(i)>='a' && passedExpressionLine.charAt(i)<='z') ||
                    passedExpressionLine.substring(i, i + 1).equals("\"")) {

                passedExpressionLine = passedExpressionLine.toUpperCase();
                //depending on wether it starts with Char or Not, it will change the state to true for the next call of the function
                if(!startsWithNumber && !startsWithChar) {
                    startPos = i;
                    startsWithChar = true;
                }
                else if(startsWithNumber) {
                    isError = true;
                }

            }
            //if the currentToken char is between 1 and 9, startsWithNumber == true for the next call of the function
            else if((passedExpressionLine.charAt(i)>='0' && passedExpressionLine.charAt(i)<='9') || passedExpressionLine.charAt(i)=='-') {
                if(!startsWithNumber && !startsWithChar) {
                    startPos = i;
                    startsWithNumber = true;
                }
            }

            else if(passedExpressionLine.charAt(i)=='+') {
                if(!startsWithNumber && !startsWithChar) {
                    startPos = i+1;
                    startsWithNumber = true;
                }
            }
        }

        position = i;
        //return a new Token depending on the type of the ATOM
        if(startsWithNumber) {
            if(!isError)
                return new Token(passedExpressionLine.substring(startPos, i),NUMERIC_ATOM);
            else
                throw new Exception("> **error: Invalid token wrong identifier**");
        } else {
            passedExpressionLine = passedExpressionLine.replace("\"", "");
            if(passedExpressionLine.length() > 1){
                return new Token(passedExpressionLine.substring(startPos, i),STRING_ATOM);
            } else {
                return new Token(passedExpressionLine.substring(startPos, i),LITERAL_ATOM);
            }

        }

    }

}
